package com.devaunteledee.izea_interview_twitter_client.Tweet;

import android.app.Activity;
import android.os.Bundle;

import com.devaunteledee.izea_interview_twitter_client.R;

public class TweetActivity extends Activity {


    /*
     * onCreate called when activity is created
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet);

        TweetFragment fragment = TweetFragment.newInstance();

        getFragmentManager().beginTransaction().replace(R.id.tweetContainer, fragment, TweetFragment.TAG).commit();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
