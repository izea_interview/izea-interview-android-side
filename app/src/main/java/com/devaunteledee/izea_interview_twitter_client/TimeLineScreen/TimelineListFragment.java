package com.devaunteledee.izea_interview_twitter_client.TimeLineScreen;

import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;

import com.devaunteledee.izea_interview_twitter_client.Global.Global;
import com.devaunteledee.izea_interview_twitter_client.TimelineSQLDatabaseHelper.TimelineSQLDatabaseHelper;
import com.devaunteledee.izea_interview_twitter_client.TimelineService.TimelineService;

/**
 * Created by devaunteledee on 2/21/16.
 */
public class TimelineListFragment extends ListFragment {


    public static final String TAG = "TimeLineListFragment.TAG";
    private String LOG_TAG = "TimelineListFragment";
    /**
     * update database
     */

    private SQLiteDatabase timelineDB;

    //cursor for handling data

    private Cursor timelineCursor;

    // adapter for mapping data

    private TimelineListFragmentAdapter timelineAdapter;
    /**
     * broadcast receiver for when new updates are available
     */
    private BroadcastReceiver timelineBroadcastReciever;

    public static TimelineListFragment newInstance() {

        TimelineListFragment fragment = new TimelineListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupTimeline();

    }

    private void setupTimeline() {

        //retrieve the timeline

        try {

            //instantiate database helper
            TimelineSQLDatabaseHelper timelineSQLDatabaseHelper = new TimelineSQLDatabaseHelper(getActivity());
            //get the database
            timelineDB = timelineSQLDatabaseHelper.getReadableDatabase();

            //query the database, most recent tweets first
            timelineCursor = timelineDB.query("home", null, null, null, null, null, "update_time DESC");
            //manage the updates using a cursor
            getActivity().startManagingCursor(timelineCursor);
            //instantiate adapter
            timelineAdapter = new TimelineListFragmentAdapter(getActivity(), timelineCursor);
            //apply the adapter to the timeline view
            //this will make it populate the new update data in the view
            setListAdapter(timelineAdapter);

            //instantiate receiver class for finding out when new updates are available
            timelineBroadcastReciever = new TwitterUpdateReceiver();
            //register for updates
            getActivity().registerReceiver(timelineBroadcastReciever, new IntentFilter(Global.TWITTER_UPDATE));

            //start the service for updates now
            this.getActivity().startService(new Intent(this.getActivity(), TimelineService.class));
        } catch (Exception te) {
            Log.e(LOG_TAG, "Failed to fetch timeline: " + te.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            //stop the updater service
            getActivity().stopService(new Intent(getActivity(), TimelineService.class));
            //remove receiver register
            getActivity().unregisterReceiver(timelineBroadcastReciever);
            //close the database
            timelineDB.close();
        } catch (Exception se) {
            Log.e(LOG_TAG, "unable to stop service or receiver");
        }
    }

    class TwitterUpdateReceiver extends BroadcastReceiver {
        /**
         * When new updates are available, a broadcast is received here
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            //delete db rows
            int rowLimit = 100;
            if (DatabaseUtils.queryNumEntries(timelineDB, "home") > rowLimit) {
                String deleteQuery = "DELETE FROM home WHERE " + BaseColumns._ID + " NOT IN " +
                        "(SELECT " + BaseColumns._ID + " FROM home ORDER BY " + "update_time DESC " +
                        "limit " + rowLimit + ")";
                timelineDB.execSQL(deleteQuery);
            }

            timelineCursor = timelineDB.query("home", null, null, null, null, null, "update_time DESC");
            getActivity().startManagingCursor(timelineCursor);
            timelineAdapter = new TimelineListFragmentAdapter(context, timelineCursor);
            setListAdapter(timelineAdapter);
        }
    }
}
