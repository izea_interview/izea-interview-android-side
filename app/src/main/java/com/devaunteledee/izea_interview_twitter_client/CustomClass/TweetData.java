package com.devaunteledee.izea_interview_twitter_client.CustomClass;

/**
 * Created by devaunteledee on 2/21/16.
 */
public class TweetData {

    private long tweetID;

    private String tweetUser;


    public TweetData(long ID, String screenName) {
        tweetID = ID;
        tweetUser = screenName;
    }


    public long getID() {
        return tweetID;
    }


    public String getUser() {
        return tweetUser;
    }
}


