package com.devaunteledee.izea_interview_twitter_client.Tweet;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.devaunteledee.izea_interview_twitter_client.Global.Global;
import com.devaunteledee.izea_interview_twitter_client.R;

import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by devaunteledee on 2/21/16.
 */
public class TweetFragment extends Fragment implements View.OnClickListener {


    public static final String TAG = "TweetFragment.TAG";
    // TWEET EDIT TEXT TEXT
    EditText tweetTxt;

    private twitter4j.Twitter tweetTwitter;
    /**
     * the update ID for this tweet if it is a reply
     */
    private long tweetID = 0;
    /**
     * the username for the tweet if it is a reply
     */
    private String tweetName = "";

    public static TweetFragment newInstance() {
        TweetFragment tweetFragment = new TweetFragment();


        return tweetFragment;
    }
    /*
     * onCreate called when activity is created
     */

    /*
     * Call setup method when this activity starts
     */
    @Override
    public void onResume() {
        super.onResume();
        setupTweet();
    }

    /**
     * Method called whenever this Activity starts
     * - get ready to tweet
     * Sets up twitter and onClick listeners
     * - also sets up for replies
     */
    private void setupTweet() {
        //get preferences for user twitter details
        /*
      shared preferences for user twitter details
     */
        SharedPreferences tweetSharedPreference = getActivity().getSharedPreferences(Global.USER_INFORMATION, 0);

        //get user token and secret for authentication
        String userToken = tweetSharedPreference.getString(Global.USER_KEY, null);
        String userSecret = tweetSharedPreference.getString(Global.USER_SECRET, null);

        //create a new twitter configuration using user details
        Configuration twitConf = new ConfigurationBuilder()
                .setOAuthConsumerKey(Global.TWITTER_KEY)
                .setOAuthConsumerSecret(Global.TWITTER_SECRET)
                .setOAuthAccessToken(userToken)
                .setOAuthAccessTokenSecret(userSecret)
                .build();
        //create a twitter instance
        tweetTwitter = new TwitterFactory(twitConf).getInstance();

        //get any data passed to this intent for a reply
        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            //if there are extras, they represent the tweet to reply to

            //get the ID of the tweet we are replying to
            tweetID = extras.getLong("tweetID");
            //get the user screen name for the tweet we are replying to
            tweetName = extras.getString("tweetUser");
            //get a reference to the text field for tweeting
            EditText theReply = (EditText) getActivity().findViewById(R.id.tweettext);
            //start the tweet text for the reply @username
            theReply.setText("@" + tweetName + " ");
            //set the cursor to the end of the text for entry
            theReply.setSelection(theReply.getText().length());

        } else {
            EditText theReply = (EditText) getActivity().findViewById(R.id.tweettext);
            theReply.setText("");
        }


        //set up listener for send tweet button
        Button tweetButton = (Button) getActivity().findViewById(R.id.dotweet);
        tweetButton.setOnClickListener(this);

    }

    /**
     * Listener method for button clicks
     * - for home button and send tweet button
     */
    public void onClick(View v) {

        tweetTxt = (EditText) getActivity().findViewById(R.id.tweettext);
        //find out which view has been clicked
        switch (v.getId()) {
            case R.id.dotweet:
                if (!Global.isNetworkAvailable(v.getContext())) {
                    Toast.makeText(getActivity(), "Please connect to a network in order to send a tweet", Toast.LENGTH_SHORT).show();
                } else {
                    //send tweet - get the text
                    String toTweet = tweetTxt.getText().toString();
                    try {
                        //is a reply
                        if (tweetName.length() > 0) {
                            if (toTweet.length() < 141) {
                                tweetTwitter.updateStatus(new StatusUpdate(toTweet).inReplyToStatusId(tweetID));
                                tweetTxt.setText("");
                                getActivity().finish();
                                Toast.makeText(getActivity(), "Reply sent to " + tweetName, Toast.LENGTH_SHORT).show();


                            } else {
                                Toast.makeText(getActivity(), "Tweet must contain no more than 140 characters", Toast.LENGTH_SHORT).show();
                            }
                        }
                        //is a normal tweet
                        else {
                            if (toTweet.length() < 141 && toTweet.length() > 0) {
                                tweetTwitter.updateStatus(toTweet);
                                tweetTxt.setText("");
                                getActivity().finish();
                                Toast.makeText(getActivity(), "Tweet posted", Toast.LENGTH_SHORT).show();


                            } else {
                                Toast.makeText(getActivity(), "Tweet must contain at least 1 character & no more than 140 characters.", Toast.LENGTH_SHORT).show();

                            }
                        }

                    } catch (TwitterException te) {
                        Log.e("NiceTweet", te.getMessage());
                    }

                    break;
                }

            default:
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.send_tweet_layout, container, false);
        setHasOptionsMenu(true);


        return view;


    }
}
