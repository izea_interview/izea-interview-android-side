package com.devaunteledee.izea_interview_twitter_client.TimelineService;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.devaunteledee.izea_interview_twitter_client.Global.Global;
import com.devaunteledee.izea_interview_twitter_client.TimelineSQLDatabaseHelper.TimelineSQLDatabaseHelper;

import java.util.List;

import twitter4j.Status;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by devaunteledee on 2/21/16.
 */
public class TimelineService extends Service {

    //delay between fetching new tweets

    private static int mins = 5;
    /**
     * shared preferences for user details
     */
    private static final long FETCH_DELAY = mins * (60 * 1000);

    // handler for updater

    private Handler timelineHandler;
    private SQLiteDatabase timelineDatabase;
    /**
     * updater thread object
     */
    private TimelineUpdater niceUpdater;
    /**
     * twitter object
     */
    private twitter4j.Twitter timelineTwitter;

    @Override
    public void onCreate() {
        super.onCreate();
        //get prefs
        //get database helper
        /*
      database helper object
     */
        TimelineSQLDatabaseHelper timelineSQLDatabaseHelper = new TimelineSQLDatabaseHelper(this);
        //get the database
        timelineDatabase = timelineSQLDatabaseHelper.getWritableDatabase();
        //get user preferences
        /*
      timeline database
     */
        SharedPreferences twitterUserPrefrences = getSharedPreferences(Global.USER_INFORMATION, 0);

        String userToken = twitterUserPrefrences.getString(Global.USER_KEY, null);

        Log.i("USER TOKEN: " + userToken, ".......");
//
        String userSecret = twitterUserPrefrences.getString(Global.USER_SECRET, null);
        Log.i("USER SECRET: " + userSecret, "");

        //create new configuration
        Configuration twitConf = new ConfigurationBuilder()
                .setOAuthConsumerKey(Global.TWITTER_KEY)
                .setOAuthConsumerSecret(Global.TWITTER_SECRET)
                .setOAuthAccessToken(userToken)
                .setOAuthAccessTokenSecret(userSecret)
                .build();

        //instantiate new twitter
        timelineTwitter = new TwitterFactory(twitConf).getInstance();
    }

    /*
     * Get ready for updating
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStart(intent, startId);
        //get handler
        timelineHandler = new Handler();

        //create an instance of the updater class
        niceUpdater = new TimelineUpdater();
        //add to run queue
        timelineHandler.post(niceUpdater);
        //return sticky
        return START_STICKY;
    }

    /**
     * Perform close tasks
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        //stop the updating
        timelineHandler.removeCallbacks(niceUpdater);
        timelineDatabase.close();
    }

    /*
     * Bind method required but we do not need to do anything in it
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * TimelineUpdater class implements the runnable interface
     */
    class TimelineUpdater implements Runnable {
        //run method
        public void run() {

            //check for updates - assume none
            boolean statusChanges = false;
            try {
                //retrieve the new home timeline tweets as a list
                List<Status> homeTimeline = timelineTwitter.getHomeTimeline();
                //iterate through new status updates
                for (Status statusUpdate : homeTimeline) {
                    //call the getValues method of the data helper class, passing the new updates
                    ContentValues timelineValues = TimelineSQLDatabaseHelper.getValues(statusUpdate);
                    timelineDatabase.insertOrThrow("home", null, timelineValues);
                    //confirm we have new updates
                    statusChanges = true;
                }
            } catch (Exception te) {
                String LOG_TAG = "TimelineService";
                Log.e(LOG_TAG, "Exception: " + te);
            }
            //if we have new updates, send a broadcast
            if (statusChanges) {
                //this should be received in the main timeline class
                sendBroadcast(new Intent(Global.TWITTER_UPDATE));
            }
            //delay fetching new updates
            timelineHandler.postDelayed(this, FETCH_DELAY);
        }
    }
}
