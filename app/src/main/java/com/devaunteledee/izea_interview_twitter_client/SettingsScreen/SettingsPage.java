package com.devaunteledee.izea_interview_twitter_client.SettingsScreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.devaunteledee.izea_interview_twitter_client.Global.Global;
import com.devaunteledee.izea_interview_twitter_client.LoginScreen.LoginActivity;
import com.devaunteledee.izea_interview_twitter_client.R;
import com.devaunteledee.izea_interview_twitter_client.TimeLineScreen.TimelineActivity;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;

public class SettingsPage extends Activity implements View.OnClickListener {
    Button settingButton;
    Twitter tweetTwitter;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_page);


        settingButton = (Button) findViewById(R.id.logoutButton);
        settingButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // BRINGS UP A ALERT DIALOG BUILDER TO PROMPT IF YOU WOULD LIKE TO LOG OUT OR NOT

        if (v.getId() == R.id.logoutButton) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);


            alertDialogBuilder.setTitle("Logout?");

            alertDialogBuilder
                    .setMessage("Are you sure you want to log out?")
                    .setCancelable(false)
                    .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // IF CLICKED YOU GET LOGGED OUT AND SENT BACK TO THE LOGIN PAGE.
                            clearCredentials();
                            Intent intent = new Intent(SettingsPage.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            finish();
                            TimelineActivity timelineActivity = new TimelineActivity();
                            timelineActivity.finish();

                            startActivity(intent);

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // CANCELS THE DIALOG
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();


        }
    }

    private void clearCredentials() {
        SharedPreferences tweetSharedPreference = getSharedPreferences(Global.USER_INFORMATION, 0);
        tweetTwitter = new TwitterFactory().getInstance();

        final Editor edit = tweetSharedPreference.edit();
        edit.remove(Global.USER_KEY);
        edit.remove(Global.USER_SECRET);
        edit.putBoolean(Global.TWITTER_LOGIN_CHECK, false);

        edit.commit();

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SettingsPage.this, TimelineActivity.class);
        finish();

        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imageView = (ImageView) findViewById(R.id.background_image);
        Glide.with(this).load(R.drawable.izea_background_image).into(imageView);

    }
}
