package com.devaunteledee.izea_interview_twitter_client.TimeLineScreen;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.devaunteledee.izea_interview_twitter_client.CustomClass.TweetData;
import com.devaunteledee.izea_interview_twitter_client.Global.Global;
import com.devaunteledee.izea_interview_twitter_client.R;
import com.devaunteledee.izea_interview_twitter_client.Tweet.TweetActivity;

import java.net.URL;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by devaunteledee on 2/21/16.
 */
public class TimelineListFragmentAdapter extends SimpleCursorAdapter {


    /**
     * strings representing database column names to map to views
     */
    static final String[] databaseColumnNames = {"update_text", "user_screen", "update_time", "user_img"};
    /**
     * view item IDs for mapping database record values to
     */
    static final int[] tweetItemIDs = {R.id.updateText, R.id.userScreen, R.id.updateTime, R.id.userImg};

    private String LOG_TAG = "UpdateAdapter";


    // constructor sets up adapter, passing 'from' data and 'to' views
    /**
     * tweetListener handles clicks of reply and retweet buttons
     * - also handles clicking the user name within a tweet
     */
    private View.OnClickListener tweetListener = new View.OnClickListener() {
        //onClick method
        public void onClick(final View v) {
            //which view
            switch (v.getId()) {
                //reply button pressed
                case R.id.reply:

                    //create an intent for sending a new tweet
                    Intent replyIntent = new Intent(v.getContext(), TweetActivity.class);
                    //get the data from the tag within the button view
                    TweetData tweetData = (TweetData) v.getTag();
                    //pass the status ID
                    replyIntent.putExtra(Global.TWEET_ID, tweetData.getID());
                    //pass the user name
                    replyIntent.putExtra(Global.TWEET_USER, tweetData.getUser());
                    //go to the tweet screen
                    v.getContext().startActivity(replyIntent);
                    break;

                //retweet button pressed
                case R.id.retweet:
                    if (!Global.isNetworkAvailable(v.getContext())) {

                        Toast.makeText(v.getContext(), "Please connect to a network to retweet selected tweet", Toast.LENGTH_SHORT).show();

                    } else {
                        new AlertDialog.Builder(v.getContext())
                                .setTitle("Retweet?")
                                .setIcon(R.mipmap.ic_launcher)
                                .setMessage("Are you sure you want to retweet this tweet?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        //get context
                                        Context appCont = v.getContext();
                                        //get preferences for user access
                                        SharedPreferences tweetSharedPreferences = appCont.getSharedPreferences(Global.USER_INFORMATION, 0);
                                        String userToken = tweetSharedPreferences.getString(Global.USER_KEY, null);
                                        String userSecret = tweetSharedPreferences.getString(Global.USER_SECRET, null);
                                        //create new Twitter configuration
                                        Configuration twitConf = new ConfigurationBuilder()
                                                .setOAuthConsumerKey(Global.TWITTER_KEY)
                                                .setOAuthConsumerSecret(Global.TWITTER_SECRET)
                                                .setOAuthAccessToken(userToken)
                                                .setOAuthAccessTokenSecret(userSecret)
                                                .build();
                                        //create Twitter instance for retweeting
                                        Twitter retweetTwitter = new TwitterFactory(twitConf).getInstance();
                                        //get tweet data from view tag
                                        TweetData data = (TweetData) v.getTag();
                                        try {
                                            //retweet, passing the status ID from the tag
                                            retweetTwitter.retweetStatus(data.getID());
                                            //confirm to use
                                            CharSequence text = "Retweeted!";
                                            int duration = Toast.LENGTH_SHORT;
                                            Toast toast = Toast.makeText(appCont, text, duration);
                                            toast.show();
                                        } catch (TwitterException te) {
                                            Log.e(LOG_TAG, te.getMessage());
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(v.getContext(), "Retweet Canceled", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                    break;
                //user has pressed tweet user name
                case R.id.userScreen:

                    if (!Global.isNetworkAvailable(v.getContext())) {
                        Toast.makeText(v.getContext(), "Please connect to a network to retweet selected tweet", Toast.LENGTH_SHORT).show();
                    } else {
                        //When the user presses the user name within a tweet they are takento the user's profile page in the browser/ Or App.
                        //get the user screen name
                        TextView tv = (TextView) v.findViewById(R.id.userScreen);
                        String userScreenName = tv.getText().toString();
                        //open the user's profile page in the browser
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://twitter.com/" + userScreenName));
                        v.getContext().startActivity(browserIntent);
                        break;
                    }
                default:
                    break;
            }
        }
    };


    public TimelineListFragmentAdapter(Context context, Cursor cursor) {
        super(context, R.layout.tweet_layout, cursor, databaseColumnNames, tweetItemIDs);
    }


    @Override
    public void bindView(View row, Context context, Cursor cursor) {
        super.bindView(row, context, cursor);


        try {
            //get profile image
            URL profileURL = new URL(cursor.getString(cursor.getColumnIndex("user_img")));
            //set the image in the view for the current tweet
            ImageView profPic = (ImageView) row.findViewById(R.id.userImg);
            Glide.with(context).load(profileURL).into(profPic);

        } catch (Exception te) {
            Log.e(LOG_TAG, te.getMessage());
        }

        //get the update time
        long createdAt = cursor.getLong(cursor.getColumnIndex("update_time"));
        //get the update time view
        TextView textCreatedAt = (TextView) row.findViewById(R.id.updateTime);
        //adjust the way the time is displayed to make it readable
        textCreatedAt.setText(DateUtils.getRelativeTimeSpanString(createdAt) + " ");

        // For retweets and replies, we need to store the tweet ID and user screen name in the view


        //get the status ID
        long statusID = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
        //get the user name
        String statusName = cursor.getString(cursor.getColumnIndex("user_screen"));
        //create a StatusData object to store these
        TweetData tweetData = new TweetData(statusID, statusName);

        //set the status data object as tag for both retweet and reply buttons in this view
        row.findViewById(R.id.retweet).setTag(tweetData);
        row.findViewById(R.id.reply).setTag(tweetData);
        //setup onclick listeners for the retweet and reply buttons
        row.findViewById(R.id.retweet).setOnClickListener(tweetListener);
        row.findViewById(R.id.reply).setOnClickListener(tweetListener);
        //setup  onclick for the user screen name within the tweet
        row.findViewById(R.id.userScreen).setOnClickListener(tweetListener);
    }

}


