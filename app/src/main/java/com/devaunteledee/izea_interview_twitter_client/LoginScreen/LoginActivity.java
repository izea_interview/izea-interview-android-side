package com.devaunteledee.izea_interview_twitter_client.LoginScreen;

import android.app.Activity;
import android.os.Bundle;

import com.devaunteledee.izea_interview_twitter_client.R;

public class LoginActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        LoginFragment fragment = LoginFragment.newInstance();

        getFragmentManager().beginTransaction().replace(R.id.loginContainer, fragment, LoginFragment.TAG).commit();

    }
}
