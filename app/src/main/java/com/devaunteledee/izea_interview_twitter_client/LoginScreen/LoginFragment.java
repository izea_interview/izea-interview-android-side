package com.devaunteledee.izea_interview_twitter_client.LoginScreen;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.devaunteledee.izea_interview_twitter_client.Global.Global;
import com.devaunteledee.izea_interview_twitter_client.R;
import com.devaunteledee.izea_interview_twitter_client.TimeLineScreen.TimelineActivity;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;


/**
 * Created by devaunteledee on 2/24/16.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {

    public static final int WEBVIEW_REQUEST_CODE = 100;
    public static final String TAG = "TweetFragment.TAG";
    Button loginButton;
    ImageView imageView;
    private Twitter twitter;
    private RequestToken twitterRequestToken;
    private SharedPreferences twitterUserPrefrences;
    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;

    public static LoginFragment newInstance() {
        LoginFragment loginFragment = new LoginFragment();


        return loginFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_layout, container, false);
        setHasOptionsMenu(true);


        return view;


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loginButton = (Button) getActivity().findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);
        initTwitterConfigs();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        twitterUserPrefrences = getActivity().getSharedPreferences(Global.USER_INFORMATION, 0);

        boolean isLoggedIn = twitterUserPrefrences.getBoolean(Global.TWITTER_LOGIN_CHECK, false);

        if (isLoggedIn) {
            Intent timelineIntent = new Intent(getActivity(), TimelineActivity.class);

            startActivity(timelineIntent);
            getActivity().finish();

        } else {

            Uri uri = getActivity().getIntent().getData();

            if (uri != null && uri.toString().startsWith(callbackUrl)) {

                String verifier = uri.getQueryParameter(oAuthVerifier);

                try {

                    AccessToken accessToken = twitter.getOAuthAccessToken(twitterRequestToken, verifier);

                    saveTwitterInfo(accessToken);

                    Intent timelineIntent = new Intent(getActivity(), TimelineActivity.class);

                    startActivity(timelineIntent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initTwitterConfigs() {
        consumerKey = Global.TWITTER_KEY;
        consumerSecret = Global.TWITTER_SECRET;
        callbackUrl = Global.TWITTER_CALLBACK;
        oAuthVerifier = Global.VERIFIER;
    }

    private void saveTwitterInfo(AccessToken accessToken) {


        SharedPreferences.Editor e = twitterUserPrefrences.edit();
        e.putString(Global.USER_KEY, accessToken.getToken());
        e.putString(Global.USER_SECRET, accessToken.getTokenSecret());
        e.putBoolean(Global.TWITTER_LOGIN_CHECK, true);
        e.commit();

    }


    private void loginToTwitter() {

        boolean isLoggedIn = twitterUserPrefrences.getBoolean(Global.TWITTER_LOGIN_CHECK, false);

        if (!isLoggedIn) {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                twitterRequestToken = twitter.getOAuthRequestToken(callbackUrl);

                final Intent intent = new Intent(getActivity(), LoginWebViewActivity.class);
                intent.putExtra(LoginWebViewActivity.EXTRA_URL, twitterRequestToken.getAuthenticationURL());
                startActivityForResult(intent, WEBVIEW_REQUEST_CODE);
            } catch (TwitterException e) {
                e.printStackTrace();
            }
        } else {
            Intent timelineIntent = new Intent(getActivity(), TimelineActivity.class);

            startActivity(timelineIntent);
            getActivity().finish();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            String verifier = data.getExtras().getString(oAuthVerifier);

            try {
                AccessToken accessToken = twitter.getOAuthAccessToken(twitterRequestToken, verifier);


                saveTwitterInfo(accessToken);


                Intent timelineIntent = new Intent(getActivity(), TimelineActivity.class);

                startActivity(timelineIntent);
                getActivity().finish();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.loginButton) {

            if (!Global.isNetworkAvailable(getActivity())) {
                Toast.makeText(getActivity(), "Please connect to a network to authenticate with twitter ", Toast.LENGTH_SHORT).show();
            } else {
                loginToTwitter();

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        imageView = (ImageView) getActivity().findViewById(R.id.background_image);
        Glide.with(this).load(R.drawable.izea_background_image).into(imageView);

    }
}
