package com.devaunteledee.izea_interview_twitter_client.LoginScreen;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.devaunteledee.izea_interview_twitter_client.Global.Global;
import com.devaunteledee.izea_interview_twitter_client.R;

/**
 * Created by devaunteledee on 2/22/16.
 */
public class LoginWebViewActivity extends Activity {

    public static String EXTRA_URL = "extra_url";
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_web_view);

        final String url = this.getIntent().getStringExtra(EXTRA_URL);

        if (url == null) {
            finish();
        }

        webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl(url);
    }

    class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.contains(Global.TWITTER_CALLBACK)) {

                Uri uri = Uri.parse(url);

                String verifier = uri.getQueryParameter("oauth_verifier");
                Intent resultIntent = new Intent();
                resultIntent.putExtra("oauth_verifier", verifier);
                setResult(RESULT_OK, resultIntent);

                finish();
                return true;
            }
            return false;
        }
    }
}
