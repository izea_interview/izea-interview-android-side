package com.devaunteledee.izea_interview_twitter_client.Global;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by devaunteledee on 2/21/16.
 */
public class Global {
    /**
     * twitter authentication key
     */
    public static final String TWITTER_KEY = "Xv4ZGIXT0FHI1lyK5lPweL4cm";
    /**
     * twitter secret
     */
    public static final String TWITTER_SECRET = "LXEO2Z0gBadRjH5PUCbYmhOHNJhu5xZhD8fbI3f2VLKYTDX0Qf";
    //BROADCAST RECIEVER STRING TO UPDATE FEED

    public static final String TWITTER_UPDATE = "TWITTER_UPDATES";

    // STRING OF SHARED PREFRENCE TO CHECK IF USER IS LOGGED INTO TWITTER OR NOT
    public static final String TWITTER_LOGIN_CHECK = "IS_LOGGED_INTO_TWITTER?";

    //  TWITTER VERIFIER STRING
    public static final String VERIFIER = "oauth_verifier";

    // USER KEY STRING
    public static final String USER_KEY = null;

    // USER SECRET KEY STRING
    public static final String USER_SECRET = "";

    // SHARED PREFRENCE STRING
    public static final String USER_INFORMATION = "";
    // TWITTER CALLBACK URL
    public static final String TWITTER_CALLBACK = "http://dledee1995.android.app";

    // TWEET ID STRING
    public static final String TWEET_ID = "tweetID";

    // TWEET USER STRING
    public static final String TWEET_USER = "tweetUser";

    // NETWORK CHECK

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        boolean isAvailable = false;

        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }

        return isAvailable;
    }

}
